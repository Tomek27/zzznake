# ZZZnake
This is a simple console based game. The goal is to collect the coin on the field and than get to the door. There will be a snake approaching the player with every step he takes.

## Java -> Rust
This game is a reimplementation of this Java program: [ZZZZZnake](http://openbook.rheinwerk-verlag.de/javainsel/03_005.html)
