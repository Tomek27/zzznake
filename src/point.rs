use std::cmp::PartialEq;
use std::fmt;

#[derive(Debug, Clone)]
pub struct Point {
    pub line: usize,
    pub row: usize,
}

impl Point {
    pub fn new(line: usize, row: usize) -> Self {
        Point { line, row }
    }
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "({}, {})", self.line, self.row)
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.line == other.line && self.row == other.row
    }
}
