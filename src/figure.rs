use std::fmt;

use point::Point;

macro_rules! impl_figure {
    ($name:ident, $repr:expr) => {
        #[derive(Debug, Clone)]
        pub struct $name {
            pub position: Option<Point>,
        }

        impl $name {
            pub fn new(pos: Point) -> Self {
                $name {
                    position: Some(pos)
                }
            }

            pub fn position(&self) -> Option<Point> {
                self.position.clone()
            }

            pub fn set_position(&mut self, pos: Point) {
               self.position = Some(pos);
            }

            pub fn remove(&mut self) {
                self.position.take();
            }
        }

        impl fmt::Display for $name {
            fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
                write!(f, $repr)
            }
        }
    }
}

impl_figure!(Coin, "@");
impl_figure!(Door, "#");
impl_figure!(Snake, "$");
impl_figure!(Player, "&");
