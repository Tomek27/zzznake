use std::default::Default;

pub static DEFAULT_FIELD_WIDTH: usize = 40;
pub static DEFAULT_FIELD_HEIGHT: usize = 20;

#[derive(Debug, Clone)]
pub struct Field {
    pub width: usize,
    pub height: usize,
}

impl Field {
    pub fn new(width: usize, height: usize) -> Self {
        Field { width, height }
    }
}

impl Default for Field {
    fn default() -> Self {
        Field {
            width: DEFAULT_FIELD_WIDTH,
            height: DEFAULT_FIELD_HEIGHT,
        }
    }
}
