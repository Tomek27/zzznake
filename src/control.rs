pub enum Key {
    Up,
    Down,
    Left,
    Right,
    Enter,
}

pub fn map(key: u8) -> Option<Key> {
    match key {
        // up arrow or w
        27 | 119 => Some(Key::Up),
        // down arrow or s
        28 | 115 => Some(Key::Down),
        // left arrow or a
        29 | 97 => Some(Key::Left),
        // right arrow or d
        30 | 100 => Some(Key::Right),
        // enter
        10 => Some(Key::Enter),
        _ => None,
    }
}
