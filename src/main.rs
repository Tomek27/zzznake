extern crate nix;

use nix::sys::termios;
use std::io::Read;

mod control;
mod field;
mod figure;
mod game;
mod point;

fn main() {
    println!("=== ZZZnake ===");

    // Querying original as a separate, since `Termios` does not implement copy
    let orig_term = termios::tcgetattr(0).unwrap();
    let mut term = termios::tcgetattr(0).unwrap();

    // Unset canonical mode, so we get characters immediately
    term.local_flags.remove(termios::LocalFlags::ICANON);

    // Don't generate signals on Ctrl-C and friends
    term.local_flags.remove(termios::LocalFlags::ISIG);

    // Disable local echo
    term.local_flags.remove(termios::LocalFlags::ECHO);

    termios::tcsetattr(0, termios::SetArg::TCSADRAIN, &term).unwrap();

    let mut game = game::Game::default();
    game.print();

    // Game loop
    for byte in std::io::stdin().bytes() {
        let byte = byte.unwrap();
        if byte == 3 {
            break;
        } else {
            // Process player input
            game.input(byte);
            if game.won() {
                println!("You have won :)");
                break;
            } else if game.lost() {
                println!("You looser :P");
                break;
            }
        }
        game.print();
    }

    println!("Goodbye!");
    // Reset old terminal
    termios::tcsetattr(0, termios::SetArg::TCSADRAIN, &orig_term).unwrap();
}
