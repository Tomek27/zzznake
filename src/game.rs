use control;
use control::Key;
use field::Field;
use figure::{Coin, Door, Player, Snake};
use point::Point;

use std::cmp::min;
use std::default::Default;
use std::fmt;

pub struct Game {
    field: Field,
    player: Player,
    coin: Coin,
    door: Door,
    snake: Snake,
}

impl Game {
    pub fn new(field_width: usize, field_height: usize) -> Self {
        Game {
            field: Field::new(field_width, field_height),
            ..Default::default()
        }
    }

    pub fn input(&mut self, key: u8) {
        if let Some(direction) = control::map(key) {
            self.update_player_position(direction);
            if self.found_coin() { self.eat_coin() }
            self.snake_towards_player();
        }
    }

    pub fn won(&self) -> bool {
        self.coin.position().is_none() && self.player.position() == self.door.position()
    }

    pub fn lost(&self) -> bool {
        self.player.position() == self.snake.position()
    }

    pub fn print(&self) {
        println!("{}\n", self);
    }

    fn update_player_position(&mut self, direction: Key) {
        let old_pos = self.player.position().unwrap();
        let mut new_line = old_pos.line;
        let mut new_row = old_pos.row;
        match direction {
            Key::Up => new_line = old_pos.line.saturating_sub(1),
            Key::Down => new_line = min(self.field.height - 1, old_pos.line.saturating_add(1)),
            Key::Left => new_row = old_pos.row.saturating_sub(1),
            Key::Right => new_row = min(self.field.width - 1, old_pos.row.saturating_add(1)),
            _ => (),
        };
        self.player.set_position(Point::new(new_line, new_row));
    }

    fn snake_towards_player(&mut self) {
        let player = self.player.position().unwrap();
        let mut snake = self.snake.position().unwrap();

        if player.row < snake.row {
            snake.row -= 1;
        } else if player.row > snake.row {
            snake.row += 1;
        }

        if player.line < snake.line {
            snake.line -= 1;
        } else if player.line > snake.line {
            snake.line += 1;
        }

        self.snake.set_position(snake);
    }

    fn found_coin(&self) -> bool {
        self.player.position() == self.coin.position()
    }

    fn eat_coin(&mut self) {
        self.coin.remove();
    }
}

impl Default for Game {
    fn default() -> Self {
        let field = Field::default();
        let player = Player::new(Point::new(0, 0));
        let coin = Coin::new(Point::new(0, 1));
        let door = Door::new(Point::new(1, 0));
        let snake = Snake::new(Point::new(field.height - 1, field.width - 1));

        Game {
            field,
            player,
            coin,
            door,
            snake,
        }
    }
}

static EMPTY_FIELD: char = '.';

impl fmt::Display for Game {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut count = 0;
        for line in 0..self.field.height {
            for row in 0..self.field.width {
                let point = Some(Point::new(line, row));

                if point == self.player.position() {
                    write!(f, "{}", self.player)?;
                } else if point == self.coin.position() {
                    write!(f, "{}", self.coin)?;
                } else if point == self.door.position() {
                    write!(f, "{}", self.door)?;
                } else if point == self.snake.position() {
                    write!(f, "{}", self.snake)?;
                } else {
                    write!(f, "{}", EMPTY_FIELD)?;
                }
            }
            write!(f, "\n")?;
        }
        write!(f, "")
    }
}
